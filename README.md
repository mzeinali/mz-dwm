# mz-dwm
My fork of dwm

Included patches:

+ [alpha](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-alpha.diff)
+ [alternativetags](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-alternativetags.diff)
+ [centeredmaster](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-centeredmaster.diff)
+ [fibonacci](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-fibonacci.diff)
+ [fullgaps](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-fullgaps.diff)
+ [gridmode](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-gridmode.diff)
+ [pertag](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-pertag.diff)
+ [savefloats](https://github.com/mhdzli/mz-dwm/blob/master/patches/dwm-savefloates.diff)


Due to a bug in dwm which can not handle some emojis there is a temporary solution (an unofficial patch that I added to dwm.c):

+ [Ignore-Xft-errors-when-drawing-text.patch](patches/Ignore-Xft-errors-when-drawing-text.patch)

You can read more about this issue in [here](https://groups.google.com/forum/#!topic/wmii/7bncCahYIww).

Example screenshot:

![https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm.png](https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm.png)
![https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm2.png](https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm2.png)
![https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm3.png](https://github.com/mhdzli/mz-dwm/blob/master/mz-dwm3.png)
